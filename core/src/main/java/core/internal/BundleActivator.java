package core.internal;

import core.SettingService;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;
import java.util.Properties;

/**
 * Extension of the default OSGi bundle activator
 */
public final class BundleActivator
        implements org.osgi.framework.BundleActivator {

    private static Logger logger = LoggerFactory.getLogger(BundleActivator.class);

    /**
     * Called whenever the OSGi framework starts our bundle
     */
    public void start(BundleContext bc)
            throws Exception {
        logger.info("STARTING core bundle");

        Dictionary props = new Properties();

        logger.info("REGISTER core.SettingService");
        bc.registerService(SettingService.class.getName(), new SettingServiceImpl(), props);
    }

    /**
     * Called whenever the OSGi framework stops our bundle
     */
    public void stop(BundleContext bc)
            throws Exception {
        logger.info("STOPPING core bundle");
        logger.info("UNREGISTER core.SettingService");
        //System.out.println( "STOPPING core" );

        // no need to unregister our service - the OSGi framework handles it for us
    }
}

