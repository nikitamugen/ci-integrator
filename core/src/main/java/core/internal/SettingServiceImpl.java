package core.internal;

import core.SettingCode;
import core.SettingService;
import core.exception.EmptySettingException;
import core.exception.UnknownSettingCodeException;
import core.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

public class SettingServiceImpl implements SettingService {

    private static String defaultPropValue = "";
    private static int defaultConnectionPoolSize = 1;

    private static Logger logger = LoggerFactory.getLogger(SettingServiceImpl.class);

    private Properties datasourceProperties;
    private Properties slackProperties;

    public SettingServiceImpl() {
        super();

        logger.info("INITIALIZE service");

        datasourceProperties = new Properties();
        slackProperties = new Properties();

        loadPropertiesByResourceName(datasourceProperties, "datasource.properties");
        loadPropertiesByResourceName(slackProperties, "slack.properties");
    }

    private void loadPropertiesByResourceName(Properties propertiesLink, String resourceName) {

        logger.info("LOAD resource " + resourceName);

        InputStream input = null;

        try {
            input = getClass().getResourceAsStream(resourceName);
            propertiesLink.load(input);

        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage(), ex);

        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    logger.error(ex.getLocalizedMessage());
                    ex.printStackTrace();
                }
            }
        }
    }

    public Serializable getSettingByCode(SettingCode settingCode)
            throws EmptySettingException, UnknownSettingCodeException, NullPointerException {

        if (settingCode == null) {
            throw new NullPointerException();
        }

        switch (settingCode) {
            case slackToken:
                return getSlackToken();
            case slackMainChannel:
                return getSlackMainChannel();

            case jenkinsHost:
                return getJenkinsHost();
            case jenkinsUserName:
                return getJenkinsUserName();
            case jenkinsUserToken:
                return getJenkinsUserToken();

            case hibernateProperties:
                return getHibernateProperties();
            case hibernateDialect:
                return getHibernateDialect();
            case hibernateShowSql:
                return getHibernateShowSql();
            case hibernateHBM2DDLAuto:
                return getHibernateHBM2DDLAuto();
            case hibernateCacheProviderClass:
                return getHibernateCacheProviderClass();

            case jdbcConnectionPoolSize:
                return getJdbcConnectionPoolSize();

            case databaseConnectionUrl:
                return getDatabaseConnectionUrl();
            case databaseDriverClassName:
                return getDatabaseDriverClassName();
            case databaseConnectionPassword:
                return getDatabaseConnectionPassword();
            case databaseConnectionUserName:
                return getDatabaseConnectionUserName();
            case databaseConnectionDefaultSchema:
                return getDatabaseConnectionDefaultSchema();

            default:
                throw new UnknownSettingCodeException(settingCode);
        }
    }

    private String getDatabaseDriverClassName() throws EmptySettingException {
        final String propName = "database.connection.driver_class";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getDatabaseConnectionUrl() throws EmptySettingException {
        final String propName = "database.connection.url";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getDatabaseConnectionUserName() {
        final String propName = "database.connection.username";
        String prop = datasourceProperties.getProperty(propName, defaultPropValue);

        return prop;
    }

    private String getDatabaseConnectionPassword() {
        String propName = "database.connection.password";
        String prop = datasourceProperties.getProperty(propName, defaultPropValue);

        return prop;
    }

    private String getDatabaseConnectionDefaultSchema() {
        final String propName = "database.connection.default_schema";
        String prop = datasourceProperties.getProperty(propName, defaultPropValue);

        return prop;
    }

    private int getJdbcConnectionPoolSize() {
        final String propName = "jdbc.connection.pool_size";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            return defaultConnectionPoolSize;
        }

        return Integer.parseInt(prop);
    }

    private String getHibernateDialect() throws EmptySettingException {
        final String propName = "hibernate.dialect";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getHibernateShowSql() throws EmptySettingException {
        final String propName = "hibernate.show_sql";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getHibernateHBM2DDLAuto() throws EmptySettingException {
        final String propName = "hibernate.hbm2ddl.auto";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getHibernateCacheProviderClass() throws EmptySettingException {
        final String propName = "hibernate.cache.provider_class";
        String prop = datasourceProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private Properties getHibernateProperties() throws EmptySettingException {
        final Properties hibernateProperties = new Properties();

        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", getHibernateHBM2DDLAuto());
        hibernateProperties.setProperty("hibernate.dialect", getHibernateDialect());
        hibernateProperties.setProperty("hibernate.show_sql", getHibernateShowSql());

        return hibernateProperties;
    }

    private String getSlackToken() throws EmptySettingException {
        final String propName = "slack.token";
        String prop = slackProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getSlackMainChannel() throws EmptySettingException {
        final String propName = "slack.mainChannel";
        String prop = slackProperties.getProperty(propName);
        if (StringUtils.isEmpty(prop)) {
            throw new EmptySettingException(propName);
        }

        return prop;
    }

    private String getJenkinsHost() {
        return "nikitamugen.ru:8080";
    }

    private String getJenkinsUserName() {
        return "watcher";
    }

    private String getJenkinsUserToken() {
        return "b6e0518b93f9cf44a76aae9c40eb9382";
    }
}
