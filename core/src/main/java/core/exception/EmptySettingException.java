package core.exception;

public class EmptySettingException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private static final String message = "[EMPTY SETTING] ";
	
	public EmptySettingException(String prop) {
		super(message + prop);
	}
	
	
}
