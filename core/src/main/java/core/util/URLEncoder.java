package core.util;

import java.io.UnsupportedEncodingException;

public class URLEncoder {

    private static final String codePage = "UTF-8";

    public String encodeBody(String body) {
        String encodedBody = "";
        try {
            encodedBody = java.net.URLEncoder.encode(body, codePage);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodedBody;
    }
}
