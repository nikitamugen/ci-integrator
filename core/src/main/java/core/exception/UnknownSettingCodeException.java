package core.exception;

import core.SettingCode;

public class UnknownSettingCodeException extends Exception {

    private static final long serialVersionUID = 1L;
    private static final String message = "[UNKNOWN SETTING] ";

    public UnknownSettingCodeException(SettingCode settingCode) {
        super(message + settingCode.toString());
    }
}
