package core;

import core.exception.EmptySettingException;
import core.exception.UnknownSettingCodeException;

import java.io.Serializable;

public interface SettingService {

    Serializable getSettingByCode(SettingCode settingCode)
            throws EmptySettingException, UnknownSettingCodeException, NullPointerException;

}
