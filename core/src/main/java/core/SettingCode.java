package core;

public enum SettingCode {
    databaseDriverClassName,
    databaseConnectionUrl,
    databaseConnectionUserName,
    databaseConnectionPassword,
    databaseConnectionDefaultSchema,

    jdbcConnectionPoolSize,

    hibernateDialect,
    hibernateShowSql,
    hibernateHBM2DDLAuto,
    hibernateCacheProviderClass,
    hibernateProperties,

    slackToken,
    slackMainChannel,

    jenkinsHost,
    jenkinsUserName,
    jenkinsUserToken
}
